**Generate the QR code from the avatar and the URL of the profile page.**

Generate a QR code that can access your Mastodon profile page.
You can place your avatar (like user icon) in the middle part of the QR code.

---

## Installation

You can install MastodonQRGenerator according to the following procedure.

1. Please download ZIP file from 'Download'.
2. Please execute "python(python3 in Ubuntu) setup.py install" in the cloned directory.
3. Running "mastodon_generate_qr" on the terminal starts the interactive program.

---

## Requirements

When using MastodonQRGenerator, the following requirements must be satisfied.

- Python3

The latest stable version of these software is preferable.

---

## Plans

MastodonQRGenerator will use SQLite for the database.
