# -*- code: utf-8 -*-
"""Get access token from Mastodon instance and generate QR code using avatar.

After obtaining the access token using MAuth,
use QRGen to generate the QR code using the avatar.

"""


from mauth import au
from qrgen import qr


def main():
    """Get access token and generate QR code."""
    print('Please enter user name.')
    username = input('>>> @')
    print('Please enter server domain.(e.g. mstdn.jp)')
    server_domain = input('>>> ')
    print('Issue an access token from the Mastodon instance.')
    access_token = au.main(server_domain)
    print('Access token issuance completed.')
    print('Generate QR code.')
    qr.main(server_domain, username, access_token)
    print('QR code generation is complete. Check in the img directory.')


if __name__ == "__main__":
    main()
