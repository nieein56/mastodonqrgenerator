# -*- code: utf-8 -*-
"""Get access_token from Mastodon instance.

Get client_id and client_secret from Mastodon instance, issue access_token
after creating authorization URL.

"""

import sqlite3
import requests
from urllib.parse import urlencode

CONN = sqlite3.connect('client.db')
CLIENT_NAME = 'Mastodon QRCode Generator'
SCOPE = 'read:accounts'


def _get_client_id(server_domain, c):
    """Get client_id and client_secret from Mastodon instance.

    Get client_id and client_secret from Mastodon instance.
    If you have already acquired them, use the existing ones.

    Args:
        server_domain (str): The domain of the Mastodon instance.

    Returns:
        client_id: The acquired client_id.
        client_secret: The acquired client_secret.

    """
    # If the access token has not been acquired, newly acquired.
    payload = {'client_name': CLIENT_NAME,
               'redirect_uris': 'urn:ietf:wg:oauth:2.0:oob',
               'scopes': SCOPE}
    res = requests.post('https://' + server_domain + '/api/v1/apps',
                        params=payload).json()

    client_id = res["client_id"]
    client_secret = res["client_secret"]

    return client_id, client_secret


def _get_authorize_url(client_id, server_domain):
    """Create authorization URL.

    Create a URL for authorization using client_id.

    Args:
        client_id (str): The acquired client_id.
        server_domain (str): The domain of the Mastodon instance.

    Returns:
        url: The authority authentication URL created.

    """
    params = urlencode(dict(
        client_id=client_id,
        response_type="code",
        redirect_uri="urn:ietf:wg:oauth:2.0:oob",
        scope=SCOPE
    ))

    return 'https://' + server_domain + '/oauth/authorize?' + params


def _get_access_token(client_id, client_secret, code, server_domain, c):
    """Get access_token from Mastodon instance.

    Using accessed client_id, client_secret and authentication code,
    obtain access_token.

    Args:
        client_id (str): The acquired client_id.
        client_secret (str): The acquired client_secret.
        code (str): The obtained authentication code.
        server_domain (str): The domain of the Mastodon instance.

    Returns:
        access_token: The acquired access_token.

    """
    payload = {'grant_type': 'authorization_code',
               'redirect_uri': 'urn:ietf:wg:oauth:2.0:oob',
               'client_id': client_id,
               'client_secret': client_secret,
               'code': code}
    res = requests.post('https://' + server_domain + '/oauth/token',
                        params=payload).json()
    access_token = res["access_token"]

    client = (server_domain, access_token)
    c.execute('INSERT INTO clients (server_domain, access_token) values(?, ?)',
              client)
    CONN.commit()
    CONN.close()

    return access_token


def main(server_domain):
    """Use the generated function, obtain access_token."""
    c = CONN.cursor()
    c.execute("""CREATE TABLE IF NOT EXISTS clients (
              server_domain TEXT, access_token TEXT)""")
    c.execute("""SELECT COUNT(*) FROM
              clients WHERE server_domain='""" + server_domain + """'""")
    if c.fetchone()[0] != 0:
        print('An access token was found in the database.')
        for val in c.execute("""SELECT access_token FROM clients
                    WHERE server_domain='""" + server_domain + """'"""):
            access_token = val[0]
        CONN.close()

        print("access_token:", access_token)
        return access_token
    else:
        client_id, client_secret = _get_client_id(server_domain, c)

        print("client_id:    ", client_id)
        print("client_secret:", client_secret)

        url = _get_authorize_url(client_id, server_domain)

        print("Please open the following URL in the browser.")
        print(url)
        print("Please enter the displayed authentication code.")
        code = input(">>> ")

        access_token = _get_access_token(client_id, client_secret,
                                         code, server_domain, c)

        print("access_token:", access_token)
        return access_token
