# -*- code: utf-8 -*-
"""Generate QR code for Mastodon user."""
import requests
import random
import string
import tempfile
import os
import qrcode
from PIL import Image

temp = tempfile.TemporaryDirectory()


def random_filename():
    """Generate filename for avatar."""
    randlst = [random.choice(string.ascii_letters
                             + string.digits) for i in range(16)]
    return ''.join(randlst)


def get_user_avatar(server_address, access_token):
    """Get user avatar from Mastodon instance."""
    url = server_address + '/api/v1/accounts/verify_credentials'
    if os.path.exists('./img') is False:
        os.mkdir('./img')
    headers = {'Authorization': 'Bearer ' + access_token}
    res = requests.get(url, headers=headers).json()
    avatar_url = res["avatar_static"]
    avatar_url = avatar_url.split('?')[0]
    r = requests.get(avatar_url, stream=True)
    ext = os.path.splitext(url)[1]
    filename = random_filename()
    avatar_path = temp.name + filename + ext
    if r.status_code == 200:
        fp = open(avatar_path, 'wb')
        fp.write(r.content)
        fp.close
    return avatar_path


def gen_qr_code(server_address, username, avatar_path):
    """Generate QR code for Mastodon user's page."""
    if os.path.exists('./img') is False:
        os.mkdir('./img')
    url = server_address + '/@' + username
    avatar = Image.open(avatar_path).resize((80, 80), Image.BICUBIC)
    qr_big = qrcode.QRCode(
        version=6,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
    )
    qr_big.add_data(url)
    qr_big.make()
    img_qr_big = qr_big.make_image().convert('RGB')

    pos = ((img_qr_big.size[0] - avatar.size[0]) // 2,
           (img_qr_big.size[1] - avatar.size[1]) // 2)

    img_qr_big.paste(avatar, pos)
    img_qr_big.save('img/' + username + '.png')


def main(server_domain, username, access_token):
    """Wowowow."""
    server_address = 'https://' + server_domain
    avatar_path = get_user_avatar(server_address, access_token)
    gen_qr_code(server_address, username, avatar_path)
    temp.cleanup()
