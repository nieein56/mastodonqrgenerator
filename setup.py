# -*- code: utf-8 -*-
"""Used for package installation."""
from setuptools import setup

setup(
    name='mqrgen',
    version='0.2.1',
    description='Get access token from Mastodon instance and generate QR code using avatar.',
    author='Nie(sha)',
    author_email='general@nieein56.xyz',
    url='https://bitbucket.org/nieein56/mastodonqrgenerator/',
    packages=['mauth', 'qrgen'],
    py_modules=['exe'],
    install_requires=[
        "requests>=2.0.0",
        "pillow>=5.0.0",
        "qrcode>=5.0"
    ],
    entry_points={
        "console_scripts": [
            "mastodon_generate_qr=exe:main"
        ]
    }
)
